/**
Copyright (C) 2012, 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use anyhow::Result;
use mwapi_responses::prelude::*;
use mwbot::{parsoid::*, ApiClient, Bot};
use tracing::{error, info};

#[derive(Debug)]
struct CategoryMember {
    talk_title: String,
    main_title: String,
}

#[query(
    prop = "info",
    inprop = "associatedpage",
    generator = "categorymembers",
    gcmlimit = "max"
)]
struct CategoryMemberResponse {}

/// Query the current protection status of a page
async fn category_members(
    name: &str,
    api: &ApiClient,
) -> Result<Vec<CategoryMember>> {
    let mut params = CategoryMemberResponse::params().to_vec();
    params.push(("gcmtitle", name));
    let result: CategoryMemberResponse = api.get(&params).await?;
    let status: Vec<_> = result
        .items()
        .iter()
        .map(|page| CategoryMember {
            talk_title: page.title.to_string(),
            main_title: page.associatedpage.to_string(),
        })
        .collect();
    Ok(status)
}

#[query(prop = "revisions", rvprop = "ids", rvlimit = "max")]
struct IdsResponse;

async fn get_revision_ids(name: &str, api: &ApiClient) -> Result<Vec<u32>> {
    let mut params = IdsResponse::params().to_vec();
    params.push(("titles", name));
    let result: IdsResponse = api.get(&params).await?;
    let revs: Vec<_> = result
        .items()
        .iter()
        .flat_map(|page| page.revisions.iter().map(|rev| rev.revid))
        .collect();
    Ok(revs)
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().init();
    let bot = Bot::from_default_config().await?;
    let members = category_members(
        "Category:Good articles without an oldid",
        bot.get_api(),
    )
    .await?;
    let mut handles = vec![];
    for member in members {
        let bot = bot.clone();
        handles.push(tokio::spawn(async move {
            match handle_page(&member, &bot).await {
                Ok(_) => {}
                Err(err) => error!("{} failed: {:?}", &member.talk_title, err),
            };
        }));
    }
    for handle in handles {
        handle.await.unwrap();
    }

    info!("Finished successfully");
    Ok(())
}

async fn handle_page(member: &CategoryMember, bot: &Bot) -> Result<()> {
    info!("Looking up page={}", &member.main_title);

    let revisions = get_revision_ids(&member.main_title, bot.get_api()).await?;
    let mut ever_found = false;
    let mut previous: Option<u32> = None;
    let page = bot.get_page(&member.main_title);
    for revision in revisions {
        info!("{}: Looking at id={}", &member.main_title, &revision);
        let html = page.get_revision_html(revision).await?;
        let found = html
            .inclusive_descendants()
            .filter_map(|node| node.as_indicator())
            .any(|temp| temp.get_name().unwrap() == "good-star");
        if found {
            ever_found = true;
            previous = Some(revision);
            continue;
        } else if !ever_found {
            info!(
                "{}: Doesn't have Template:Good article, skipping",
                &member.main_title
            );
            return Ok(());
        }

        // Did not find the <indicator>
        if let Some(revid) = previous {
            // This is the one!
            info!("Template was added in id={}", revid);
            break;
        }
    }

    let oldid = match previous {
        Some(id) => id,
        None => {
            return Ok(());
        }
    };

    let talk_page = bot.get_page(&member.talk_title);
    let edit = {
        let talk_html = talk_page.get_html().await?;
        let mut found_template = false;
        for template in talk_html.filter_templates()? {
            if template.name() == "Template:GA" {
                template.set_param("oldid", &oldid.to_string())?;
                found_template = true;
                break;
            }
        }

        if !found_template {
            info!("Unable to find Template:GA on page={}", &member.talk_title);
            return Ok(());
        }
        talk_page.prep_saving_html(&talk_html)?
    };
    talk_page
        .save(
            edit.set_summary("Bot: Setting |oldid= for {{[[Template:GA|GA]]}}"),
        )
        .await?;

    Ok(())
}
